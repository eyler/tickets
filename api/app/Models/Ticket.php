<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    use HasFactory;

    /**
     * Relaciones que se cargan por defecto
     */

    protected $with = ['customer'];

    /**
     * Obtener el cliente dueño del boleto.
     */
    public function customer()
    {
        return $this->belongsTo('App\Models\Customer');
    }

    /**
     * Obtener el campo de fecha formateado.
     *
     * @return string
     */
    public function getDateAttribute($value)
    {
        $date = new Carbon($value);
        return $date->locale(app()->getLocale())->translatedFormat('l d, F Y');
    }
}
