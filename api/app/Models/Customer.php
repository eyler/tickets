<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * Atributos agregados
     */
    protected $appends = ['full_name'];

    /**
     * Obtener el nombre completo del cliente.
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    /**
     * Obtener los boletos de este cliente.
     */
    public function tickets()
    {
        return $this->hasMany('App\Models\Ticket');
    }
}
