<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;

class ApiLocalization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Verifique la solicitud del encabezado y determine la localización
        $local = ($request->hasHeader('X-localization')) ? $request->header('X-localization') : 'es';
        // establecer la localización
        app()->setLocale($local);
        Carbon::SetLocale($local);
        // Continuar con la solicitud
        return $next($request);
    }
}
