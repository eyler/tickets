<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CustomerController extends Controller
{
    /**
     * Obtenemos todos los clientes
     */
    public function getCustomers(Request $request)
    {
        $customers = Customer::paginate(8);
        return response($customers, 200);
    }

    /**
     * Nuevo cliente
     */
    public function createCustomer(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string|max:100',
            'last_name' => 'required|string|max:100',
            'identification' => 'required|integer|unique:customers',
            'phone' => 'required|integer|digits_between:6,15|min:100000',
            'email' => 'required|email|unique:customers|max:191',
            'age' => 'required|integer|digits_between:1,3|min:8|max:100'
        ]);
        if ($validator->fails()) {
            return response($validator->errors()->all(), 422);
        }
        $customer = new Customer();
        $customer->first_name = $request->first_name;
        $customer->last_name = $request->last_name;
        $customer->identification = $request->identification;
        $customer->phone = $request->phone;
        $customer->email = $request->email;
        $customer->age = $request->age;
        $customer->save();
        return response(['data' => $customer, 'message' => 'Successfully created customer'], 201);
    }

    /**
     * Eliminar cliente
     */
    public function deleteCustomer(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'customer' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return response($validator->errors()->all(), 422);
        }

        $customer = Customer::find($request->customer);

        if ($customer != null) {
            $customer->delete();
            return response(['data' => '', 'message' => 'Successfully deleted customer'], 200);
        } else {
            return response('This customer does not exist', 404);
        }
    }
}
