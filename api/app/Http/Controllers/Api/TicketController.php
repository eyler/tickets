<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Ticket;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TicketController extends Controller
{
    /**
     * Obtenemos todos los boletos
     */
    public function getTickets(Request $request)
    {
        $tickets = Ticket::paginate(8);
        return response($tickets, 200);
    }

    /**
     * Obtenemos nuevos  boletos
     */
    public function newTickets(Request $request)
    {

        $tickets = Ticket::where('status', 1)->count();
        if ($tickets >= 20) {
            return response('You can only load a maximum of 20 unassigned tickets, assign tickets to load new ones.', 400);
        } else {
            for ($i = $tickets; $i < 20; $i++) {
                $ticket = new Ticket();
                $ticket->date = Carbon::now()->addDays(10)->format('Y-m-d H:i:s');
                $ticket->min_age = rand(8, 30);
                $ticket->save();
            }
        }
        return response(Ticket::paginate(8), 200);
    }

    /**
     * Asignar boleto
     */
    public function assignTicket(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'ticket' => 'required|integer',
            'identification' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return response($validator->errors()->all(), 422);
        }

        $customer = Customer::where('identification', $request->identification)->first();

        if ($customer != null) {
            $ticket = Ticket::find($request->ticket);
            if ($ticket != null) {
				if($ticket->customer_id==null){
					// $ticket->date = date('Y-m-d H:i:s');
					if ($ticket->min_age > $customer->age) {
						return response('The customer does not meet the minimum age', 422);
					} else {
						$ticket->status = 2;
						$ticket->customer_id = $customer->id;
						$ticket->save();
						return response(['data' => $ticket, 'message' => 'Ticket assigned successfully'], 201);
					}
				}else{
					return response('Taken', 422);
				}
            } else {
                return response('This ticket does not exist', 404);
            }
        } else {
            return response('This customer does not exist', 404);
        }
    }
}
