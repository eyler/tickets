<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use GuzzleHttp\Client as Curl;

class AuthController extends Controller
{
    /**
     * Metodo para iniciar sesión y recuperar un token
     *
     * @var Request $request
     *
     * @return json $response
     *  */
    public function login(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6',
        ]);
        if ($validator->fails()) {
            return response($validator->errors()->all(), 422);
        }
        $http = new Curl();
        try {

            $response = $http->post(config('services.passport.login_endpoint'), [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => config('services.passport.client_id'),
                    'client_secret' => config('services.passport.client_secret'),
                    'username' => $request->email,
                    'password' => $request->password,
                ]
            ]);

            return $response->getBody();
        } catch (BadResponseException $e) {

            if ($e->getCode() === 400 && ((!isset($request->email) && !isset($request->password)) || ($request->email == null && $request->password == null))) {
                return response('Invalid Request. Please enter a username or a password.', $e->getCode());
            } else if ($e->getCode() === 401) {
                return response('Your credentials are incorrect. Please try again.', $e->getCode());
            }

            return response('Something went wrong on the server.', $e->getCode());
        }
    }

    /**
     * Cerrar sesión
     *
     * @return json $response
     */
    public function logout()
    {
        auth()->user()->tokens->each(function ($token, $key) {
            $token->delete();
        });

        return response('You have been successfully logged out.', 200);
    }



    /**
     * Usuario actual
     *
     * @return json $response
     */
    public function user()
    {
        return response(auth()->user(), 200);
    }
}
