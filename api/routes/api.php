<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\CustomerController;
use App\Http\Controllers\Api\TicketController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function () {
    Route::group(['middleware' => ['cors', 'locale', 'json.response']], function () {
        Route::post('/login', [AuthController::class, 'login'])->name('login.api');
        Route::middleware('auth:api')->group(function () {
            // nuestras rutas a proteger con autenticación entrarán aquí
            Route::get('/user', [AuthController::class, 'user'])->name('user.api');
            Route::get('/tickets', [TicketController::class, 'getTickets'])->name('tickets.api');
            Route::get('/customers', [CustomerController::class, 'getCustomers'])->name('customers.api');
            Route::post('/new-customer', [CustomerController::class, 'createCustomer'])->name('new-customer.api');
            Route::post('/delete-customer', [CustomerController::class, 'deleteCustomer'])->name('delete-customer.api');
            Route::post('/news-tickets', [TicketController::class, 'newTickets'])->name('new-tickets.api');
            Route::post('/assign-tickets', [TicketController::class, 'assignTicket'])->name('assing-tickets.api');
            Route::post('/logout', [AuthController::class, 'logout'])->name('logout.api');
        });
    });
});
