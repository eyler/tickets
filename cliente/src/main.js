import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import i18n from "./i18n";
import "./index.css";
import "./transition.css";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faHome,
  faSignOutAlt,
  faSignInAlt,
  faTicketAlt,
  faSun,
  faMoon,
  faCheckCircle,
  faTimesCircle,
  faCopyright,
  faGlobeAmericas,
  faBars,
  faLock,
  faSpinner,
  faTimes,
  faUsers,
  faUserPlus,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

library.add(
  faHome,
  faSignOutAlt,
  faSignInAlt,
  faTicketAlt,
  faSun,
  faMoon,
  faCheckCircle,
  faTimesCircle,
  faCopyright,
  faGlobeAmericas,
  faBars,
  faLock,
  faSpinner,
  faTimes,
  faUsers,
  faUserPlus
);

const app = createApp(App);

app.component("Fa", FontAwesomeIcon);
app.use(i18n).use(store).use(router).mount("#app");
