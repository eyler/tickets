import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/Home.vue";
import Tickets from "../views/Tickets.vue";
import Customers from "../views/Customers.vue";
import CreateCustomer from "../views/CreateCustomer.vue";
import Login from "../views/Login.vue";
import store from "../store";
import i18n from "../i18n";

const authGuard = (to, from, next) => {
  if (store.getters.isAuthenticated) {
    next();
  } else {
    next("/login");
  }
};

const noAuth = (to, from, next) => {
  if (!store.getters.isAuthenticated) {
    next();
  } else {
    next("/");
  }
};

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
    beforeEnter: authGuard,
  },
  {
    path: "/tickets",
    name: "Tickets",
    component: Tickets,
    beforeEnter: authGuard,
  },
  {
    path: "/customers",
    name: "Customers",
    component: Customers,
    beforeEnter: authGuard,
  },
  {
    path: "/new-customer",
    name: "New customer",
    component: CreateCustomer,
    beforeEnter: authGuard,
  },
  {
    path: "/login",
    name: "Login",
    component: Login,
    beforeEnter: noAuth,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

// Clear the error on every navigation
router.afterEach((to, from) => {
  document.title = i18n.global.t(to.name || "Boletos");
  store.commit("clearError");
});

export default router;
