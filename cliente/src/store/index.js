import { createStore } from "vuex";
import createHttp from "../services/http";
import router from "../router";
import theme from "../modules/theme";
import lang from "../modules/lang";

export default createStore({
  state: {
    tickets: {
      data: [],
    },
    customers: {
      data: [],
    },
    token: localStorage.getItem("token") || "",
    expiration: Date.now(),
    isBusy: false,
    error: "",
    success: "",
  },
  mutations: {
    setTickets: (state, tickets) => (state.tickets = tickets),
    setCustomers: (state, customers) => (state.customers = customers),
    setBusy: (state) => (state.isBusy = true),
    clearBusy: (state) => (state.isBusy = false),
    setError: (state, error) => (state.error = error),
    clearError: (state) => (state.error = ""),
    setSuccess: (state, success) => (state.success = success),
    clearSuccess: (state) => (state.success = ""),
    setToken: (state, token) => {
      state.token = token;
      localStorage.setItem("token", token);
    },
    clearToken: (state) => {
      state.token = "";
      localStorage.setItem("token", "");
    },
  },
  getters: {
    isAuthenticated: (state) => state.token.length > 0,
  },
  actions: {
    loadTickets: async ({ commit }, page = 1) => {
      try {
        commit("setBusy");
        commit("clearError");
        const http = createHttp(); // secured
        const tickets = await http.get("tickets?page=" + page);
        commit("setTickets", tickets.data);
      } catch (error) {
        // console.log(error);
        error = Array.isArray(error.response.data)
          ? error.response.data[0]
          : error.response.data;
        commit("setError", error);
      } finally {
        commit("clearBusy");
      }
    },
    loadCustomers: async ({ commit }, page = 1) => {
      try {
        commit("setBusy");
        commit("clearError");
        const http = createHttp(); // secured
        const customers = await http.get("customers?page=" + page);
        commit("setCustomers", customers.data);
      } catch (error) {
        // console.log(error);
        error = Array.isArray(error.response.data)
          ? error.response.data[0]
          : error.response.data;
        commit("setError", error);
      } finally {
        commit("clearBusy");
      }
    },
    newTickets: async ({ commit, dispatch }) => {
      try {
        commit("setBusy");
        commit("clearError");
        const http = createHttp(); // secured
        const tickets = await http.post("news-tickets");
        commit("setTickets", tickets.data);
      } catch (error) {
        // console.log(error);
        error = Array.isArray(error.response.data)
          ? error.response.data[0]
          : error.response.data;
        commit("setError", error);
      } finally {
        commit("clearBusy");
      }
    },
    assignTicket: async ({ commit }, model) => {
      try {
        commit("setBusy");
        commit("clearError");
        commit("clearSuccess");
        const http = createHttp(); // secured
        const tickets = await http.post("assign-tickets", model);
        commit("setSuccess", tickets.data.message);
        return tickets.status;
      } catch (error) {
        // console.log(error);
        var er = Array.isArray(error.response.data)
          ? error.response.data[0]
          : error.response.data;
        commit("setError", er);
        return error.response.status;
      } finally {
        commit("clearBusy");
      }
    },
    createCustomer: async ({ commit }, model) => {
      try {
        commit("setBusy");
        commit("clearError");
        commit("clearSuccess");
        const http = createHttp(); // secured
        const customer = await http.post("new-customer", model);
        commit("setSuccess", customer.data.message);
        return customer.status;
      } catch (error) {
        // console.log(error);
        var er = Array.isArray(error.response.data)
          ? error.response.data[0]
          : error.response.data;
        commit("setError", er);
        return error.response.status;
      } finally {
        commit("clearBusy");
      }
    },
    deleteCustomer: async ({ commit }, model) => {
      try {
        commit("setBusy");
        commit("clearError");
        commit("clearSuccess");
        const http = createHttp(); // secured
        const customer = await http.post("delete-customer", model);
        commit("setSuccess", customer.data.message);
        return customer.status;
      } catch (error) {
        // console.log(error);
        var er = Array.isArray(error.response.data)
          ? error.response.data[0]
          : error.response.data;
        commit("setError", er);
        return error.response.status;
      } finally {
        commit("clearBusy");
      }
    },
    login: async ({ commit }, model) => {
      try {
        commit("setBusy");
        commit("clearError");
        const http = createHttp(false); // unsecured
        const result = await http.post("login", model);

        commit("setToken", result.data.access_token);
        // console.log(router.push("/"));
        router.push("/");
      } catch (error) {
        error = Array.isArray(error.response.data)
          ? error.response.data[0]
          : error.response.data;
        commit("setError", error);
      } finally {
        commit("clearBusy");
      }
    },
    logout: async ({ commit }) => {
      try {
        commit("setBusy");
        commit("clearError");
        const http = createHttp(); // secured
        const result = await http.post("logout");
      } catch (error) {
        console.log(error);
        error = Array.isArray(error.response.data)
          ? error.response.data[0]
          : error.response.data;
        commit("setError", error);
      } finally {
        commit("clearBusy");
        commit("clearToken");
        router.push("/login");
      }
    },
  },
  modules: {
    theme,
    lang,
  },
});
