﻿import axios from "axios";
import store from "../store";
const baseURL = "http://api.tickets.test/api/v1/";

export default function createHttp(secured = true) {
  if (secured) {
    const Axios = axios.create({
      baseURL: baseURL,
      headers: {
        Authorization: `Bearer ${store.state.token}`,
        "X-localization": localStorage.lang ? localStorage.lang : "es",
      },
    });
    Axios.interceptors.response.use(
      (response) => {
        return response;
      },
      (error) => {
        if (
          401 === error.response.status ||
          (500 === error.response.status &&
            error.response.config.url === "user")
        ) {
          if (store.getters.isAuthenticated) {
            store.dispatch("logout");
          }
          return Promise.resolve(error.response);
        } else {
          return Promise.reject(error);
        }
      }
    );
    return Axios;
  } else {
    return axios.create({
      baseURL: baseURL,
      headers: {
        "X-localization": localStorage.lang ? localStorage.lang : "es",
      },
    });
  }
}
