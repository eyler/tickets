import { createI18n } from "vue-i18n";
import es from "./lang/es";
const messages = {
  es: es,
};

const lang = document.documentElement.lang.substr(0, 2);

const i18n = createI18n({
  locale: lang, // establecer idioma
  fallbackLocale: "en",
  messages, //establecer mensajes locales
  silentFallbackWarn: true,
  silentTranslationWarn: true,
  globalInjection: true,
});

export default i18n;
