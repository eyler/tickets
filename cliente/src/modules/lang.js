export default {
  state: {
    lang: {},
  },
  mutations: {
    SET_LANG(state, lang) {
      state.lang = lang;
      localStorage.lang = lang;
    },
  },
  actions: {
    initLang({ commit }) {
      const cachedlang = localStorage.lang ? localStorage.lang : false;
      if (cachedlang) commit("SET_LANG", cachedlang);
      else commit("SET_LANG", "es");
    },
    toggleLang({ commit }, lang) {
      switch (lang) {
        case "en":
          commit("SET_LANG", "en");
          break;

        default:
          commit("SET_LANG", "es");
          break;
      }
    },
  },
  getters: {
    getLang: (state) => {
      return state.lang;
    },
  },
};
